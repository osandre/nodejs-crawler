var Crawler = require("simplecrawler");

var crawler =  Crawler("http://www.rfm.fr/");

/*
var Database =  require('./database');


var database = new Database(
    "localhost",
    "root",
    null,
    "simple-crawler"
)

database.connect()

let sql = "select * from url";
*/

var mysql = require('mysql');

let databaseParams = {
    connectionLimit : 10,
    host            : "localhost",
    user            : "root",
    password        : null,
    database        : "simple-crawler"
}
var pool  = mysql.createPool(databaseParams);

urlList = {};

function addUrl(url, parentUrl, callback) {

    
    if (urlList[url]) {
        let urlId = urlList[url];
        let parentUrlId = urlList[parentUrl];
        pool.query('INSERT into url_tree SET ?', {url_id: urlId, parent_url_id: parentUrlId},  function (error, results, fields) {
            callback(urlId)
        });
    } else {

        pool.query('INSERT into url SET ?', {url: url},  function (error, results, fields) {
            if (error) throw error;
            console.log(results.insertId + " - " + url);

            let urlId = results.insertId;
            urlList[url] = urlId;
            if (parentUrl) {

                parentUrlId = urlList[parentUrl];

                pool.query('INSERT into url_tree SET ?', {url_id: urlId, parent_url_id: parentUrlId},  function (error, results, fields) {
                    callback(urlId)
                });
            } else {
                callback(urlId);
            }
            
        })
    }
}

function updateUrl(urlId, urlStats) {

    console.log(urlStats.stateData);
    //urlStats = "POUET";
    pool.query('UPDATE url SET stats = ?, status = ?, content_type= ?, content_length = ?, request_time = ? WHERE id = ?', [JSON.stringify(urlStats), urlStats.stateData.code , urlStats.stateData.contentType, urlStats.stateData.contentLength, urlStats.stateData.requestTime, urlId],  function (error, results, fields) {
        if (error) throw error;
        //console.log(results);
    });
}



//crawler.host = "europe1.fr";
crawler.scanSubdomains = true;
crawler.interval = 25; // 250ms
crawler.maxConcurrency = 5;
crawler.maxDepth = 0;

crawler.on("crawlstart", function() {
    console.log("\n--------------------\nCrawl starting\n--------------------\n");
});

crawler.on("fetchstart", function(queueItem) {
    console.log("\n--------------------\nfetchStart\n--------------------\n");
    //console.log( queueItem);
    //console.log("fetchStart", queueItem.url);
});

crawler.on("fetchcomplete", function(queueItem, responseBuffer, response) {
    console.log("\n--------------------\nfetchcomplete\n--------------------\n");
    
//    console.log(queueItem.url, queueItem.id, queueItem.referrer);


    updateUrl(urlList[queueItem.url], queueItem, () => {

    })
    //console.log(this.queue);

    /*
    pool.query('SELECT * from url', function (error, results, fields) {
        if (error) throw error;
        console.log(results);
        for(let i=0; i< results.length; i++) {
            console.log("Result: ", results[i].id, results[i].url);
          }

          addUrl(queueItem.url, 2, (insertId) => {
              console.log(insertId);
          }) 
      });
    */  
    /*
    console.log( queueItem);
    
    console.log("I just received %s (%d bytes)", queueItem.url, responseBuffer.length);
    console.log("It was a resource of type %s", response.headers['content-type']);
    */
});


crawler.on("discoverycomplete", function(queueItem, resources) {
    console.log("\n--------------------\ndiscoverycomplete !!!\n--------------------\n");
    //console.log(resources);

    for(let i=0; i < resources.length; i++) {
        console.log(resources[i]);

        addUrl(resources[i], queueItem.url, (insertedId)  => {});

    }

    /*

addUrl(crawler.initialURL, 0, (insertedId)  => {

        urlList[crawler.initialURL] = insertedId;
        crawler.start()
    })

    */

});

crawler.on("complete", function() {
    console.log("\n--------------------\nFinished!\n--------------------\n");

    crawler.queue.max("requestLatency", function(error, max) {
        console.log("The maximum request latency was %dms.", max);
    });
    crawler.queue.min("downloadTime", function(error, min) {
        console.log("The minimum download time was %dms.", min);
    });
    crawler.queue.avg("actualDataSize", function(error, avg) {
        console.log("The average resource size received is %d bytes.", avg);
    });
});

crawler.on("queueadd", function(queueItem, referrerQueueItem) {
    console.log("\n--------------------\nqueueadd\n--------------------\n");
    console.log(queueItem.url);
    //console.log(queueItem, referrerQueueItem)
});
crawler.on("queueduplicate", function(URLData) {
    console.log("\n--------------------\nqueueduplicate\n--------------------\n");
    console.log(URLData)
});
crawler.on("queueerror", function(error, URLData) {
    console.log("\n--------------------\nqueueerror\n--------------------\n");
    console.log(error, URLData)
});
crawler.on("robotstxterror", function(error)  {
    console.log("\n--------------------\nrobotstxterror\n--------------------\n");
console.log(error)
});
crawler.on("invaliddomain", function(queueItem) {
    console.log("\n--------------------\ninvaliddomain\n--------------------\n");
    console.log(queueItem.url)
}); 
crawler.on("fetchdisallowed", function(queueItem) {
    console.log("\n--------------------\nfetchdisallowed\n--------------------\n");
console.log(queueItem)
}); 
crawler.on("fetchprevented", function(queueItem) {
    console.log("\n--------------------\nfetchprevented\n--------------------\n");
    console.log(queueItem)
});
crawler.on("fetchconditionerror", function(queueItem, error) {
    console.log("\n--------------------\nfetchconditionerror\n--------------------\n");
    console.log(queueItem, error)
}); 
crawler.on("downloadconditionerror", function(queueItem, error) {
    console.log("\n--------------------\ndownloadconditionerror\n--------------------\n");
    console.log(queueItem, error)
}); 
crawler.on("fetchheaders", function(queueItem, responseObject) {
    console.log("\n--------------------\nfetchheaders\n--------------------\n"); 
    //console.log(queueItem, responseObject);
}); 
crawler.on("cookieerror", function(queueItem, error, setCookieHeader) {
    console.log("\n--------------------\ncookieerror\n--------------------\n");
    console.log(queueItem, error, setCookieHeader)
});
crawler.on("fetchredirect", function(oldQueueItem, redirectQueueItem, responseObject) {
    console.log("\n--------------------\nfetchredirect\n--------------------\n");
    console.log(oldQueueItem, redirectQueueItem, responseObject)
});
crawler.on("fetch404", function(queueItem, responseObject) {
    console.log("\n--------------------\nfetch404\n--------------------\n");
    console.log(queueItem.url)

    updateUrl(urlList[queueItem.url], queueItem, () => {

    })

    
}); 
crawler.on("fetch410", function(queueItem, responseObject) {
    console.log("\n--------------------\nfetch410\n--------------------\n");
    //console.log(queueItem, responseObject)
    updateUrl(urlList[queueItem.url], queueItem, () => {

    })
});
crawler.on("fetchdataerror", function(queueItem, responseObject) {
    console.log("\n--------------------\nfetchdataerror\n--------------------\n");
    //console.log(queueItem, responseObject)
    updateUrl(urlList[queueItem.url], queueItem, () => {

    })
}); 
crawler.on("fetchtimeout", function(queueItem, crawlerTimeoutValue) {
    console.log("\n--------------------\nfetchtimeout\n--------------------\n");
    //console.log(queueItem, crawlerTimeoutValue)
    updateUrl(urlList[queueItem.url], queueItem, () => {

    })
});
crawler.on("fetcherror", function(queueItem, responseObject) {
    console.log("\n--------------------\nfetcherror\n--------------------\n");
    //console.log(queueItem, responseObject)

    updateUrl(urlList[queueItem.url], queueItem, () => {

    })
}); 
crawler.on("gziperror", function(queueItem, error, responseBuffer) {
    console.log("\n--------------------\ngziperror\n--------------------\n");
    //console.log(queueItem, error, responseBuffer);
    updateUrl(urlList[queueItem.url], queueItem, () => {

    })
});
crawler.on("fetchclienterror", function(queueItem, error) {
    console.log("\n--------------------\nfetchclienterror\n--------------------\n");
    //console.log(queueItem, error);
    updateUrl(urlList[queueItem.url], queueItem, () => {

    })
}); 


console.log(crawler.initialURL);

//vider les tables mysql
let insert = pool.query('DELETE from url',  function (error, results, fields) {
    if (error) throw error;
    pool.query('DELETE from url_tree',  function (error, results, fields) {
        if (error) throw error;
        
        addUrl(crawler.initialURL, null, (insertedId)  => {
            crawler.start()
        })
    
      });
  });  





