
--
-- Structure de la table `url`
--

CREATE TABLE IF NOT EXISTS `url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(512) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `content_type` varchar(255) DEFAULT NULL,
  `content_length` int(11) NOT NULL DEFAULT '0',
  `request_time` int(11) NOT NULL DEFAULT '0',
  `stats` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `url`
--


-- --------------------------------------------------------

--
-- Structure de la table `url_tree`
--

CREATE TABLE IF NOT EXISTS `url_tree` (
  `url_id` int(11) NOT NULL,
  `parent_url_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

